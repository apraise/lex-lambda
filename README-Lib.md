# Overview

A simple framework for creating [AWS lambda](https://aws.amazon.com/lambda/) powered bots for [Amazon Lex](https://aws.amazon.com/lex/).

Based on [AWS Lambdas in Scala](https://github.com/mkotsur/aws-lambda-scala), it provides typesafe codecs for the Lex
input event and response formats. It doesn't do much more.

# Usage

1. Add to your build file:

    ```
    libraryDependencies += "co.apraise" %% "lambda-lex" % version
    ```

1. Define your a handler for your bot's input:

```scala
import co.apraise.lambda.lex.payload.LexHandlerCodecs._

final class MyLexLambda extends Lambda[LexInputEvent, LexResponse] {
  override def handle(inputEvent: LexInputEvent) = Right(CloseLexResponse(CloseDialogAction(Fulfilled)))
}
```

# Motivation

The input and response formats are strongly typed. As the [Lambda Function Input Event and Response Format](http://docs.aws.amazon.com/lex/latest/dg/lambda-input-response-format.html#using-lambda-response-format)
isn't sufficiently well defined, the types serve to show what you can & can not expect to receive/return from your bot.
