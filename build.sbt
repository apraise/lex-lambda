import sbt.Keys._
//import scoverage._
import com.typesafe.sbt.S3Plugin.S3._

s3Settings

lazy val buildSettings = Seq(
  organization := "co.apraise",
  version := "0.0.1-SNAPSHOT",
  scalaVersion := "2.12.2",
  test in assembly := {}
)

lazy val circeVersion = "0.8.0"
lazy val catsVersion = "0.9.0"
lazy val mouseVersion = "0.9"
lazy val specsVersion = "3.9.1"
lazy val scalaCacheVersion = "0.9.3"
lazy val scalaUriVersion = "0.4.16"
lazy val awsLambdaVersion = "0.0.7"
lazy val slf4jVersion = "1.7.25"
lazy val logbackVersion = "1.2.3"

lazy val cats = "org.typelevel" %% "cats-core" % catsVersion
lazy val circeCore = "io.circe" %% "circe-core" % circeVersion
lazy val circeGeneric = "io.circe" %% "circe-generic" % circeVersion
lazy val circeParser = "io.circe" %% "circe-parser" % circeVersion
lazy val mouse = "com.github.benhutchison" %% "mouse" % mouseVersion
lazy val scalaUri = "com.netaporter" %% "scala-uri" % scalaUriVersion
lazy val awsLambda = "io.github.mkotsur" %% "aws-lambda-scala" % awsLambdaVersion
lazy val slf4jApi = "org.slf4j" % "slf4j-api" % slf4jVersion
lazy val logback = "ch.qos.logback" % "logback-classic" % logbackVersion
lazy val specsCore = "org.specs2" %% "specs2-core" % specsVersion
lazy val specsScalacheck = "org.specs2" %% "specs2-scalacheck" % specsVersion

// https://tpolecat.github.io/2014/04/11/scalac-flags.html
lazy val compilerOptions = Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:reflectiveCalls",
  "-unchecked",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Xfuture",
  "-Xlint",
  //"-Yno-predef",
  //"-Ywarn-unused-import", // gives false positives
  "-Xfatal-warnings",
  "-Ywarn-value-discard",
  "-Ypartial-unification"
)

lazy val dependencies = Seq(
  cats,
  circeCore,
  circeGeneric,
  circeParser,
  mouse,
  scalaUri,
  awsLambda,
  slf4jApi,
  logback
)

lazy val testDependencies = Seq(
  specsCore,
  specsScalacheck
)

lazy val baseSettings = Seq(
  libraryDependencies ++= testDependencies.map(_ % "test"),
  resolvers ++= Seq(
    Resolver.jcenterRepo,
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots"),
    "Twitter" at "http://maven.twttr.com"
  ),
  scalacOptions ++= compilerOptions,
  scalacOptions in Test ++= Seq("-Yrangepos"),
  scalacOptions in(Compile, console) += "-Yrepl-class-based",
  testOptions in Test += Tests.Setup(() => System.setProperty("ENV", "test"))
)

lazy val lambdaSettings = buildSettings ++ baseSettings ++ Seq(
  libraryDependencies ++= dependencies,
  //mainClass in Compile := Some("co.apraise.lambda.lex.App"),
  //mainClass in assembly := Some("co.apraise.lambda.lex.App"),
  aggregate in run := false,
  aggregate in reStart := false,
  coverageMinimum := 25.0,
  coverageFailOnMinimum := true,
  coverageExcludedPackages := "co\\.apraise\\.util\\.spec\\.*"
)

lazy val lambdaLex = project.in(file("."))
    .settings(name := "apraise-lambda-lex", moduleName := "apraise-lambda-lex")
    .settings(lambdaSettings)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

mappings in S3.upload := Seq(
  (target.value / "scala-2.12" / "apraise-lambda-lex-assembly-0.0.1-SNAPSHOT.jar", "apraise-lambda-lex-assembly-0.0.1-SNAPSHOT.jar")
)

host in S3.upload := "apraise-production-lambda.s3.amazonaws.com"

S3.progress in S3.upload := true

shellPrompt in ThisBuild := { state =>
  s"${scala.Console.YELLOW}${Project.extract(state).currentRef.project}> ${scala.Console.RESET}"
}

// Note. We aggregate coverage as a separate command in CI, see https://github.com/scoverage/sbt-scoverage#multi-project-reports
val ciCommands = List(
  "clean",
  // TODO Re-enable this.
  //  "scalastyle",
  //  "test:scalastyle",
  "compile",
  "test:compile",
  "coverage",
  "test",
  "coverageReport",
  "stage"
)
addCommandAlias("ci", ciCommands.mkString(";", ";", ""))

val deployCommands = List(
  "assembly",
  "s3-upload"
)
addCommandAlias("deploy", deployCommands.mkString(";", ";", ""))
