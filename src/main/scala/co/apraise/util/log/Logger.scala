package co.apraise.util.log

import co.apraise.util.async.AsyncOps.runAsyncUnit
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext

final class Logger(name: String)(implicit ec: ExecutionContext) {
  private lazy val log = LoggerFactory.getLogger(name)

  def trace(s: => String): Unit =
    if (log.isTraceEnabled) {
      runAsyncUnit(log.trace(s))(ec)
    }

  def debug(s: => String): Unit =
    if (log.isDebugEnabled) {
      runAsyncUnit(log.debug(s))(ec)
    }

  def info(s: => String): Unit =
    if (log.isInfoEnabled) {
      runAsyncUnit(log.info(s))(ec)
    }

  def warn(s: => String): Unit =
    if (log.isWarnEnabled) {
      runAsyncUnit(log.warn(s))(ec)
    }

  def warn(s: => String, t: Throwable): Unit =
    if (log.isWarnEnabled) {
      runAsyncUnit(log.warn(s, t))(ec)
    }

  def error(s: => String): Unit =
    if (log.isErrorEnabled) {
      runAsyncUnit(log.error(s))(ec)
    }

  def error(s: => String, t: Throwable): Unit =
    if (log.isErrorEnabled) {
      runAsyncUnit(log.error(s, t))(ec)
    }
}
