package co.apraise.util.json

import java.nio.ByteBuffer

import io.circe.{Json, Printer}

trait JsonPrinter {
  // Note. Lex expects values returned with null values (cf. slots)
  private val printer = Printer.noSpaces.copy(dropNullKeys = false)

  final def jsonToString(json: Json): String = printer.pretty(json)

  final def jsonToByteBuffer(json: Json): ByteBuffer = printer.prettyByteBuffer(json)
}

object JsonPrinter extends JsonPrinter
