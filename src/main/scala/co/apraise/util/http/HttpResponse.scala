package co.apraise.util.http

import java.io.InputStream

import org.apache.http.client.methods.CloseableHttpResponse

import scala.io.Source

final class HttpResponse(r: CloseableHttpResponse) {
  def contentString: Option[String] = contentStream.map(i => Source.fromInputStream(i).mkString)

  def contentStream: Option[InputStream] = Option(r.getEntity).map(_.getContent)

  def close(): Unit = r.close()
}
