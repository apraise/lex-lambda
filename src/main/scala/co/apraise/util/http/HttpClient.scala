package co.apraise.util.http

import com.netaporter.uri.Uri
import org.apache.http.entity.ContentType._
import org.apache.http.impl.client.{CloseableHttpClient, HttpClientBuilder}

final class HttpClient(c: CloseableHttpClient) {
  def getJson(uri: Uri): Option[String] = executeString(new GetRequest(uri))

  def postJson(uri: Uri, content: String): Option[String] = executeString(new PostRequest(uri, content, APPLICATION_JSON))

  def close(): Unit = c.close()

  private def execute(method: HttpMethod): HttpResponse = new HttpResponse(c.execute(method.asHttpUriRequest))

  private def executeString(method: HttpMethod) = {
    val response = execute(method)
    try {
      response.contentString
    } finally {
      response.close()
    }
  }
}

object HttpClient {
  def client: HttpClient = new HttpClient(HttpClientBuilder.create.build())
}
