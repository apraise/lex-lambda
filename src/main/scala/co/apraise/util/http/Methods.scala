package co.apraise.util.http

import com.netaporter.uri.Uri
import org.apache.http.client.methods.{HttpGet, HttpPost, HttpUriRequest}
import org.apache.http.entity.{ContentType, StringEntity}

trait HttpMethod {
  def asHttpUriRequest: HttpUriRequest
}

final class GetRequest(uri: Uri) extends HttpMethod {
  override def asHttpUriRequest = new HttpGet(uri.toURI)
}

final class PostRequest(uri: Uri, content: String, contentType: ContentType) extends HttpMethod {
  override def asHttpUriRequest = {
    val post = new HttpPost(uri.toURI)
    post.setEntity(new StringEntity(content, contentType))
    post
  }
}
