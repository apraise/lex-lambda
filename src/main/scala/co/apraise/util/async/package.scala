package co.apraise.util

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors.newSingleThreadExecutor

import co.apraise.util.async.AsyncOps.shutdownExecutorService

import scala.concurrent.ExecutionContext

package object async {
  lazy val singleThreadedExecutor: ExecutorService = newSingleThreadExecutor
  lazy val singleThreadedExecutionContext: ExecutionContext = ExecutionContext.fromExecutor(singleThreadedExecutor)

  sys.addShutdownHook(shutdownExecutorService(singleThreadedExecutor))
}
