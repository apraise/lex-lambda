package co.apraise.util.async

import java.util.concurrent.ExecutorService
import java.util.concurrent.TimeUnit._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Awaitable, ExecutionContext, Future}

trait AsyncOps {
  final def runAsync[T](f: => T)(implicit ec: ExecutionContext): Future[T] = Future(f)(ec)

  final def runAsyncUnit[T](f: => T)(implicit ec: ExecutionContext): Unit = {
    runAsync(f)(ec)
    ()
  }

  final def block[T](awaitable: Awaitable[T]): awaitable.type = Await.ready(awaitable, Duration.Inf)

  final def blockUnit[T](awaitable: Awaitable[T]): Unit = {
    block(awaitable)
    ()
  }

  //noinspection ScalaStyle
  final def shutdownExecutorService(executor: ExecutorService): Unit = {
    executor.shutdown()
    try {
      executor.awaitTermination(10L, SECONDS)
    } catch {
      case _: InterruptedException => {
        Console.err.println("Interrupted while waiting for graceful shutdown, forcibly shutting down...")
        executor.shutdownNow()
      }
    }
    ()
  }
}

object AsyncOps extends AsyncOps
