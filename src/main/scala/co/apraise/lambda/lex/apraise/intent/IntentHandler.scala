package co.apraise.lambda.lex.apraise.intent

import co.apraise.lambda.lex.payload.{LexInputEvent, LexResponse}

trait IntentHandler {
  def handle(inputEvent: LexInputEvent): Either[Throwable, LexResponse]
}
