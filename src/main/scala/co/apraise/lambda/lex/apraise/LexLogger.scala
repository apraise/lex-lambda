package co.apraise.lambda.lex.apraise

import co.apraise.util.async.singleThreadedExecutionContext
import co.apraise.util.log.Logger

/**
  * More information: http://docs.aws.amazon.com/lambda/latest/dg/java-logging.html
  */
trait LexLogger {
  final lazy val log: Logger = new Logger("lambda-lex")(singleThreadedExecutionContext)
}

object LexLogger extends LexLogger
