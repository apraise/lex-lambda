package co.apraise.lambda.lex.apraise

import shapeless.tag
import shapeless.tag.@@

trait CanonicalAddressTag

package object model {
  type CanonicalAddress = String @@ CanonicalAddressTag

  def CanonicalAddress(s: String): String @@ CanonicalAddressTag = tag[CanonicalAddressTag](s)
}
