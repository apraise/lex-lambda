package co.apraise.lambda.lex.apraise.model

import cats.Show

final case class MonetaryAmount(amount: BigDecimal)

object MonetaryAmount {
  private lazy val currencyFormat = java.text.NumberFormat.getCurrencyInstance

  final val showBigDecimalCurrency: Show[BigDecimal] = Show.show(b => currencyFormat.format(b))

  final val showMonetaryAmount: Show[MonetaryAmount] = Show.show(a => showBigDecimalCurrency.show(a.amount))
}
