package co.apraise.lambda.lex.apraise.intent

import co.apraise.lambda.lex.apraise.backend.ApraiseBackend
import co.apraise.lambda.lex.apraise.model.PropertyValuation._
import co.apraise.lambda.lex.payload._

object MarketValueIntentHandler extends IntentHandler {
  private val propertyTypeSlot = SlotName("PropertyType")
  private val addressSlot = SlotName("Address")

  override def handle(inputEvent: LexInputEvent) = {
    inputEvent.invocationSource match {
      case DialogCodeHook =>
        // We don't have the address, ask the user for it
        if (!inputEvent.currentIntent.slots.contains(addressSlot)) {
          Right(promptForAddressResponse(inputEvent))
        } else {
          // We now have the address, continue to next action
          Right(DelegateLexResponse(DelegateDialogAction(inputEvent.currentIntent.slots)))
        }
      case FulfillmentCodeHook => {
        // We've got all the data, fulfill the request
        val addressSlotValue = inputEvent.currentIntent.slots.get(addressSlot)
        addressSlotValue.map { maybeAddress =>
          maybeAddress.map { address =>
            ApraiseBackend.marketValue(address).fold(
              error => Right(addressErrorResponse(inputEvent, error)),
              valuation => {
                // TODO Re-prompt?
                val message = s"${showPropertyValuation.show(valuation)}."
                Right(
                  ElicitIntentLexResponse(
                    ElicitIntentDialogAction(
                      Some(
                        Message(
                          PlainText,
                          MessageContent(message)
                        )
                      )
                    )
                  )
                )
              }
            )
          }.getOrElse(Right(promptForAddressResponse(inputEvent)))
        }.getOrElse(Right(promptForAddressResponse(inputEvent)))
      }
    }
  }

  private def addressErrorResponse(inputEvent: LexInputEvent, e: Exception) = {
    val message = Some(MessageContent(s"Sorry, there was an error '${e.getMessage}'. Please enter a different address"))
    promptForAddressResponse(inputEvent, message)
  }

  private def promptForAddressResponse(inputEvent: LexInputEvent, message: Option[MessageContent] = None) =
    ElicitSlotLexResponse(
      ElicitSlotDialogAction(
        inputEvent.currentIntent.name,
        propertyTypeSlot,
        Map(
          propertyTypeSlot -> inputEvent.currentIntent.slots.getOrElse(addressSlot, None),
          addressSlot -> None
        ),
        message.map(m => Message(PlainText, m))
      )
    )
}
