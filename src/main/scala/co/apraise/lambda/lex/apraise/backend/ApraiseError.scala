package co.apraise.lambda.lex.apraise.backend

case object UnknownError extends Exception {
  override def getMessage = "Sorry, something went wrong."
}

case object ParseError extends Exception {
  override def getMessage =
    """
      |Unable to understand that address, please use the format 'Number StreetName St, Suburb, State PostCode'.
      |
      |For example '36 Newman St, Brunswick West, VIC 3055'.
      |""".stripMargin
}
