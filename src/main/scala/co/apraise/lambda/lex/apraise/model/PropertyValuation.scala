package co.apraise.lambda.lex.apraise.model

import cats.Show
import co.apraise.lambda.lex.apraise.model.Property.showProperty
import co.apraise.lambda.lex.apraise.model.ValuationEstimate.showValuationEstimate

final case class PropertyValuation(property: Property, valuation: ValuationEstimate)

object PropertyValuation {
  final val showPropertyValuation: Show[PropertyValuation] = Show.show { v =>
    Seq(showProperty.show(v.property), " has an estimated value of ", showValuationEstimate.show(v.valuation)).mkString
  }
}
