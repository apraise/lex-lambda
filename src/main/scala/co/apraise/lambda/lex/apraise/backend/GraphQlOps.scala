package co.apraise.lambda.lex.apraise.backend

trait GraphQlOps {

  implicit private[backend] final class QuotedString(val s: String) {
    def escapeQuotes: String = s.replace("\"", "\\\"")
  }

  final def marketValueQuery(address: String): String =
    s"""
       |{
       |  marketValue(address: "$address") {
       |    property {
       |      address {
       |        canonicalForm
       |      }
       |    }
       |    estimate {
       |      amount {
       |        amount
       |      }
       |      when {
       |        epochMillis
       |      }
       |    }
       |  }
       |}
     """.stripMargin
}

object GraphQlOps extends GraphQlOps
