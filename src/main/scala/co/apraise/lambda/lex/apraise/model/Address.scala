package co.apraise.lambda.lex.apraise.model

import cats.Show

final case class Address(canonicalForm: CanonicalAddress)

object Address {
  final val showAddress: Show[Address] = Show.show(a => a.canonicalForm)
}

