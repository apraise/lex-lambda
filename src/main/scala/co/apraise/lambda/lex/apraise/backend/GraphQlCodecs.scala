package co.apraise.lambda.lex.apraise.backend

import java.time.Instant

import cats.syntax.either._
import co.apraise.lambda.lex.apraise.model._
import co.apraise.util.json.syntax._
import io.circe._

trait GraphQlCodecs {
  final val graphqlQueryEncoder: Encoder[String] = Encoder.instance(q => Json.obj("query" -> Json.fromString(q)))

  final val propertyDecoder: Decoder[Property] =
    Decoder.instance { c =>
      for {
        address <- c.downField("address").downField("canonicalForm").as[String].map(s => Address(CanonicalAddress(s)))
      } yield Property(address)
    }

  final val valuationEstimateDecoder: Decoder[ValuationEstimate] =
    Decoder.instance { c =>
      for {
        amount <- c.downField("amount").downField("amount").as[Double].map(d => MonetaryAmount(BigDecimal(d)))
        when <- c.downField("when").downField("epochMillis").as[Long].map(l => Timestamp(Instant.ofEpochMilli(l)))
      } yield ValuationEstimate(amount, when)
    }

  final val errorsDecoder: Decoder[Exception] =
    Decoder.instance { c =>
      for (typ <- c.downField("type").as[String]) yield {
        typ match {
          case "co.apraise.core.InputParseError" => ParseError
          case _ => UnknownError
        }
      }
    }

  final val propertyValuationDecoder: Decoder[PropertyValuation] =
    Decoder.instance { c =>
      for {
        property <- c.downField("marketValue").downField("property").as[Property](propertyDecoder)
        valuation <- c.downField("marketValue").downField("estimate").as[ValuationEstimate](valuationEstimateDecoder)
      } yield PropertyValuation(property, valuation)
    }

  final val marketValueResponseDecoder: Decoder[Either[Seq[Exception], PropertyValuation]] =
    Decoder.instance { c =>
      if (hasErrors(c)) {
        c.downField("errors").as[Seq[Exception]](errorsDecoder.seqDecoder).map(Either.left)
      } else {
        c.downField("data").as[PropertyValuation](propertyValuationDecoder).map(Either.right)
      }
    }

  private def hasErrors(c: HCursor) = c.downField("errors").succeeded
}

object GraphQlCodecs extends GraphQlCodecs
