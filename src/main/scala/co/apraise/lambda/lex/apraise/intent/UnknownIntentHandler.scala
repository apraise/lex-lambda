package co.apraise.lambda.lex.apraise.intent

import co.apraise.lambda.lex.payload._

object UnknownIntentHandler extends IntentHandler {
  override def handle(inputEvent: LexInputEvent) =
    Right(
      ElicitIntentLexResponse(
        ElicitIntentDialogAction(
          Some(
            Message(PlainText,
              MessageContent(
                """
                  |Welcome to Apraise, we can help you find the sale price of a property.
                  |
                  |You can ask questions like "what will this house sell for?"
                  |""".stripMargin
              )
            )
          )
        )
      )
    )
}
