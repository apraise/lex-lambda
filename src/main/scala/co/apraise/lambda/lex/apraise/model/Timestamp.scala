package co.apraise.lambda.lex.apraise.model

import java.time.Instant

import cats.Show

final case class Timestamp(instant: Instant)

object Timestamp {
  final val showTimestamp: Show[Timestamp] = Show.show(t => t.instant.toString)
}
