package co.apraise.lambda.lex.apraise.model

import cats.Show
import co.apraise.lambda.lex.apraise.model.MonetaryAmount.showMonetaryAmount
import co.apraise.lambda.lex.apraise.model.Timestamp.showTimestamp

final case class ValuationEstimate(amount: MonetaryAmount, when: Timestamp)

object ValuationEstimate {
  final val showValuationEstimate: Show[ValuationEstimate] = Show.show { v =>
    Seq(showMonetaryAmount.show(v.amount), " as at ", showTimestamp.show(v.when)).mkString
  }
}
