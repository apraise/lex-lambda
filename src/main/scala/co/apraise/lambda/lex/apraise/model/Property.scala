package co.apraise.lambda.lex.apraise.model

import cats.Show
import co.apraise.lambda.lex.apraise.model.Address.showAddress

final case class Property(address: Address)

object Property {
  final val showProperty: Show[Property] = Show.show(p => showAddress.show(p.address))
}
