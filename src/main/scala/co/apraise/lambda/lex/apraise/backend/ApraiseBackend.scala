package co.apraise.lambda.lex.apraise.backend

import co.apraise.lambda.lex.apraise.backend.GraphQlCodecs.{graphqlQueryEncoder, marketValueResponseDecoder}
import co.apraise.lambda.lex.apraise.backend.GraphQlOps.marketValueQuery
import co.apraise.lambda.lex.apraise.model.PropertyValuation
import co.apraise.util.http.HttpClient
import co.apraise.util.json.CodecOps.decode
import co.apraise.util.json.JsonPrinter.jsonToString
import com.netaporter.uri.Uri

object ApraiseBackend {
  // TODO Support environments.
  private val backendUrl = "https://apraise-api.herokuapp.com/v1/graphql"

  def marketValue(address: String): Either[Exception, PropertyValuation] = {
    val response = marketValuePost(marketValueJson(address))
    response.flatMap { content =>
      val decoded = decode(content)(marketValueResponseDecoder)
      decoded match {
        case Left(e) => Left(e)
        case Right(Left(aes)) => Left(errors(aes))
        case Right(Right(v)) => Right(v)
      }
    }
  }

  private def marketValuePost(query: String): Either[Exception, String] = {
    val client = HttpClient.client
    try {
      val content = client.postJson(Uri.parse(backendUrl), query)
      content.toRight(error("No response from Apraise"))
    } finally {
      client.close()
    }
  }

  private def error(m: String) = new Exception(m)

  private def errors(es: Seq[Exception]) = error(es.map(_.getMessage).mkString("; "))

  private def marketValueJson(address: String) = jsonToString(graphqlQueryEncoder(marketValueQuery(address)))
}
