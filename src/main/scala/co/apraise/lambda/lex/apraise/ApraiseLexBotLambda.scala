package co.apraise.lambda.lex.apraise

import co.apraise.lambda.lex.apraise.LexLogger.log
import co.apraise.lambda.lex.apraise.intent.{MarketValueIntentHandler, UnknownIntentHandler}
import co.apraise.lambda.lex.payload.LexHandlerCodecs._
import co.apraise.lambda.lex.payload.{IntentName, LexInputEvent, LexResponse}
import io.github.mkotsur.aws.handler.Lambda
import io.github.mkotsur.aws.handler.Lambda._

final class ApraiseLexBotLambda extends Lambda[LexInputEvent, LexResponse] {
  override def handle(inputEvent: LexInputEvent) = {
    log.info(s"Received input event from the lex bot: $inputEvent")
    val resp =
      if (inputEvent.currentIntent.name == IntentName("PropertyMarketValue")) {
        MarketValueIntentHandler.handle(inputEvent)
      } else {
        UnknownIntentHandler.handle(inputEvent)
      }
    log.info(s"Returning response to the lex bot: $resp")
    resp
  }
}
