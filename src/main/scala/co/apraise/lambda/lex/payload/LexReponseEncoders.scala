package co.apraise.lambda.lex.payload

import io.circe.{Encoder, Json}

/**
  * Lex JSON response encoders. Following the format here:
  * http://docs.aws.amazon.com/lex/latest/dg/lambda-input-response-format.html.
  *
  * Further information is available in the examples:
  * - http://docs.aws.amazon.com/lex/latest/dg/book-trip-detail-flow.html
  * - http://docs.aws.amazon.com/lex/latest/dg/ex1-sch-appt-info-flow-details.html
  */
trait LexReponseEncoders {
  private val sessionAttributesEncoder: Encoder[Map[SessionAttributeKey, SessionAttributeValue]] =
    Encoder.instance { sa =>
      Json.fromFields(sa.map { case (k, v) => (k, Json.fromString(v)) })
    }

  private val messageEncoder: Encoder[Message] =
    Encoder.instance { m =>
      Json.obj(
        "content" -> Json.fromString(m.content),
        "contentType" -> Json.fromString(
          m.contentType match {
            case PlainText => "PlainText"
            case Ssml => "SSML"
          }
        )
      )
    }

  private val buttonEncoder: Encoder[Button] =
    Encoder.instance { b =>
      Json.obj(
        "text" -> Json.fromString(b.text),
        "value" -> Json.fromString(b.value)
      )
    }

  private val genericAttachmentsEncoder: Encoder[GenericAttachment] =
    Encoder.instance { a =>
      Json.obj(
        "title" -> Json.fromString(a.title),
        "subTitle" -> Json.fromString(a.subTitle),
        "imageUrl" -> Json.fromString(a.imageUrl.toString),
        "attachmentLinkUrl" -> Json.fromString(a.attachmentLinkUrl.toString),
        "buttons" -> Json.fromValues(a.buttons.map(buttonEncoder.apply))
      )
    }

  private val slotsWithOptionalValueEncoder: Encoder[Map[SlotName, Option[SlotValue]]] =
    Encoder.instance { ss =>
      val kvJson = ss.map { case (k, v) =>
        val valueJson = v.map(Json.fromString).getOrElse(Json.Null)
        (k, valueJson)
      }
      Json.fromFields(kvJson)
    }

  private val responseCardEncoder: Encoder[ResponseCard] =
    Encoder.instance { rc =>
      Json.obj(
        "version" -> Json.fromInt(rc.version),
        "contentType" -> Json.fromString("application/vnd.amazonaws.card.generic"),
        "genericAttachments" -> Json.fromValues(rc.attachments.map(genericAttachmentsEncoder.apply))
      )
    }

  private val elicitIntentDialogActionEncoder: Encoder[ElicitIntentDialogAction] =
    Encoder.instance { da =>
      val requiredFields = Json.obj("type" -> Json.fromString("ElicitIntent"))
      val withMessage = da.message.fold(requiredFields) { m =>
        requiredFields.deepMerge(Json.obj("message" -> messageEncoder(m)))
      }
      da.responseCard.fold(withMessage) { rc =>
        withMessage.deepMerge(Json.obj("responseCard" -> responseCardEncoder(rc)))
      }
    }

  private val elicitSlotDialogActionEncoder: Encoder[ElicitSlotDialogAction] =
    Encoder.instance { da =>
      val requiredFields = Json.obj(
        "type" -> Json.fromString("ElicitSlot"),
        "intentName" -> Json.fromString(da.intentName),
        "slotToElicit" -> Json.fromString(da.slotToElicit),
        "slots" -> slotsWithOptionalValueEncoder(da.slots)
      )
      val withMessage = da.message.fold(requiredFields) { m =>
        requiredFields.deepMerge(Json.obj("message" -> messageEncoder(m)))
      }
      da.responseCard.fold(withMessage) { rc =>
        withMessage.deepMerge(Json.obj("responseCard" -> responseCardEncoder(rc)))
      }
    }

  private val confirmIntentDialogActionEncoder: Encoder[ConfirmIntentDialogAction] =
    Encoder.instance { da =>
      val requiredFields = Json.obj(
        "type" -> Json.fromString("ConfirmIntent"),
        "intentName" -> Json.fromString(da.intentName),
        "slots" -> slotsWithOptionalValueEncoder(da.slots)
      )
      val withMessage = da.message.fold(requiredFields) { m =>
        requiredFields.deepMerge(Json.obj("message" -> messageEncoder(m)))
      }
      da.responseCard.fold(withMessage) { rc =>
        withMessage.deepMerge(Json.obj("responseCard" -> responseCardEncoder(rc)))
      }
    }

  private val delegateDialogActionEncoder: Encoder[DelegateDialogAction] =
    Encoder.instance { da =>
      Json.obj(
        "type" -> Json.fromString("Delegate"),
        "slots" -> slotsWithOptionalValueEncoder(da.slots)
      )
    }

  private val closeDialogActionEncoder: Encoder[CloseDialogAction] =
    Encoder.instance { da =>
      val requiredFields = Json.obj(
        "type" -> Json.fromString("Close"),
        "fulfillmentState" -> Json.fromString(
          da.fulfillmentState match {
            case Fulfilled => "Fulfilled"
            case Failed => "Failed"
          }
        )
      )
      val withMessage = da.message.fold(requiredFields) { m =>
        requiredFields.deepMerge(Json.obj("message" -> messageEncoder(m)))
      }
      da.responseCard.fold(withMessage) { rc =>
        withMessage.deepMerge(Json.obj("responseCard" -> responseCardEncoder(rc)))
      }
    }

  private val elicitIntentLexResponseEncoder: Encoder[ElicitIntentLexResponse] = Encoder.instance { r =>
    val daj = Json.obj("dialogAction" -> elicitIntentDialogActionEncoder(r.dialogAction))
    r.sessionAttributes.fold(daj) { sa =>
      daj.deepMerge(Json.obj("sessionAttributes" -> sessionAttributesEncoder(sa)))
    }
  }

  private val elicitSlotLexResponseEncoder: Encoder[ElicitSlotLexResponse] = Encoder.instance { r =>
    val daj = Json.obj("dialogAction" -> elicitSlotDialogActionEncoder(r.dialogAction))
    r.sessionAttributes.fold(daj) { sa =>
      daj.deepMerge(Json.obj("sessionAttributes" -> sessionAttributesEncoder(sa)))
    }
  }

  private val confirmIntentLexResponseEncoder: Encoder[ConfirmIntentLexResponse] = Encoder.instance { r =>
    val daj = Json.obj("dialogAction" -> confirmIntentDialogActionEncoder(r.dialogAction))
    r.sessionAttributes.fold(daj) { sa =>
      daj.deepMerge(Json.obj("sessionAttributes" -> sessionAttributesEncoder(sa)))
    }
  }

  private val delegateLexResponseEncoder: Encoder[DelegateLexResponse] = Encoder.instance { r =>
    Json.obj(
      "dialogAction" -> delegateDialogActionEncoder(r.dialogAction),
      "sessionAttributes" -> sessionAttributesEncoder(r.sessionAttributes)
    )
  }

  private val closeLexResponseEncoder: Encoder[CloseLexResponse] = Encoder.instance { r =>
    val daj = Json.obj("dialogAction" -> closeDialogActionEncoder(r.dialogAction))
    r.sessionAttributes.fold(daj) { sa =>
      daj.deepMerge(Json.obj("sessionAttributes" -> sessionAttributesEncoder(sa)))
    }
  }

  final val lexResponseEncoder: Encoder[LexResponse] = Encoder.instance {
    case eilr: ElicitIntentLexResponse => elicitIntentLexResponseEncoder(eilr)
    case eslr: ElicitSlotLexResponse => elicitSlotLexResponseEncoder(eslr)
    case cilr: ConfirmIntentLexResponse => confirmIntentLexResponseEncoder(cilr)
    case dlr: DelegateLexResponse => delegateLexResponseEncoder(dlr)
    case clr: CloseLexResponse => closeLexResponseEncoder(clr)
  }
}

object LexReponseEncoders extends LexReponseEncoders
