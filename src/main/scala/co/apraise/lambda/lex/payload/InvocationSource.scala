package co.apraise.lambda.lex.payload

object InvocationSource {
  def fromString(s: String): InvocationSource = s match {
    case "DialogCodeHook" => DialogCodeHook
    case _ => FulfillmentCodeHook
  }
}

/**
  * Indicates why Amazon Lex is invoking the Lambda function.
  *
  * For more information, see: http://docs.aws.amazon.com/lex/latest/dg/programming-model.html
  */
sealed trait InvocationSource

/**
  * Amazon Lex sets this value to direct the Lambda function to initialize the function and to validate the user's
  * data input.
  *
  * The Lambda function is invoked on each user input, assuming Amazon Lex understood the user intent.
  */
case object DialogCodeHook extends InvocationSource

/**
  * Amazon Lex sets this value to direct the Lambda function to fulfill an intent.
  *
  * The Lambda function is invoked on each user input, assuming Amazon Lex understood the user intent.
  */
case object FulfillmentCodeHook extends InvocationSource
