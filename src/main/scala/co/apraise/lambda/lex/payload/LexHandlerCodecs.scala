package co.apraise.lambda.lex.payload

import io.circe.{Decoder, Encoder}

/**
  * Circe decoders & encoders for Lex request & response types, for use within your handler.
  *
  * Use as follows:
  * {{{
  * import co.apraise.lambda.lex.payload.LexHandlerCodecs._
  *
  * final class MyLexLambda extends Lambda[LexInputEvent, LexResponse] {
  *   override def handle(inputEvent: LexInputEvent) = Right(CloseLexResponse(CloseDialogAction(Fulfilled)))
  * }
  * }}}
  * *
  */
object LexHandlerCodecs {
  implicit val lexInputEventDecoder: Decoder[LexInputEvent] = LexInputEventDecoders.lexInputEventDecoder
  implicit val lexResponseEncoder: Encoder[LexResponse] = LexReponseEncoders.lexResponseEncoder
}
