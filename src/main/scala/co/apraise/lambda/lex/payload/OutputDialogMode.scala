package co.apraise.lambda.lex.payload

object OutputDialogMode {
  def fromString(s: String): OutputDialogMode = s match {
    case "Voice" => Voice
    case _ => Text
  }
}

sealed trait OutputDialogMode

case object Text extends OutputDialogMode

case object Voice extends OutputDialogMode
