package co.apraise.lambda.lex.payload

/**
  * A response to send back to Lex.
  *
  * For full details see: http://docs.aws.amazon.com/lex/latest/dg/lambda-input-response-format.html#using-lambda-response-format
  */
trait LexResponse {
  def dialogAction: DialogAction
}

/**
  * Informs Amazon Lex that the user is expected to respond with an utterance that includes an intent. For example,
  * "I want a large pizza," which indicates the OrderPizzaIntent. The utterance "large," on the other hand, is not
  * sufficient for Amazon Lex to infer the user's intent.
  */
final case class ElicitIntentLexResponse(
    dialogAction: ElicitIntentDialogAction,
    sessionAttributes: Option[Map[SessionAttributeKey, SessionAttributeValue]] = None
) extends LexResponse

/**
  * Informs Amazon Lex that the user is expected to provide a slot value in the response.
  */
final case class ElicitSlotLexResponse(
    dialogAction: ElicitSlotDialogAction,
    sessionAttributes: Option[Map[SessionAttributeKey, SessionAttributeValue]] = None
) extends LexResponse

/**
  * Informs Amazon Lex that the user is expected to give a yes or no answer to confirm or deny the current intent.
  */
final case class ConfirmIntentLexResponse(
    dialogAction: ConfirmIntentDialogAction,
    sessionAttributes: Option[Map[SessionAttributeKey, SessionAttributeValue]] = None
) extends LexResponse

/**
  * Directs Amazon Lex to choose the next course of action based on the bot configuration.
  *
  * The response must include any session attributes, and the `slots` field (of  `dialogAction`) must include all of
  * the slots specified for the requested intent. If the value of the field is unknown, you must set it to `None`. You
  * will get a `DependencyFailedException` exception if your fufillment function returns the Delegate dialog action
  * without removing any slots.
  */
final case class DelegateLexResponse(
    dialogAction: DelegateDialogAction,
    sessionAttributes: Map[SessionAttributeKey, SessionAttributeValue] = Map.empty
) extends LexResponse

/**
  * Informs Amazon Lex not to expect a response from the user. For example, "Your pizza order has been placed" does not
  * require a response.
  */
final case class CloseLexResponse(
    dialogAction: CloseDialogAction,
    sessionAttributes: Option[Map[SessionAttributeKey, SessionAttributeValue]] = None
) extends LexResponse
