package co.apraise.lambda.lex.payload

final case class Intent(name: IntentName, slots: Map[SlotName, Option[SlotValue]], status: ConfirmationStatus)

final case class Bot(name: BotName, alias: Option[BotAlias], version: BotVersion)

final case class LexInputEvent(
    currentIntent: Intent,
    bot: Bot,
    userId: UserId,
    inputTranscript: Option[InputTranscript],
    invocationSource: InvocationSource,
    outputDialogMode: OutputDialogMode,
    messageVersion: MessageVersion,
    sessionAttributes: Map[SessionAttributeKey, SessionAttributeValue]
)
