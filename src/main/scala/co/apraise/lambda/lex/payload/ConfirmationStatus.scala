package co.apraise.lambda.lex.payload

/**
  * The user response to a confirmation prompt, if there is one. For example, if Amazon Lex asks "Do you want to order
  * a large cheese pizza?," depending on the user response, the value of this field can be Confirmed or Denied.
  * Otherwise, this value of this field is Unknown.
  *
  * See: http://docs.aws.amazon.com/lex/latest/dg/lambda-input-response-format.html
  */
sealed trait ConfirmationStatus

case object Unknown extends ConfirmationStatus

case object Confirmed extends ConfirmationStatus

case object Denied extends ConfirmationStatus

object ConfirmationStatus {
  def fromString(s: String): ConfirmationStatus = s match {
    case "Confirmed" => Confirmed
    case "Denied" => Denied
    case _ => Unknown
  }
}
