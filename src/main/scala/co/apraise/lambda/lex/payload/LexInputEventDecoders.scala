package co.apraise.lambda.lex.payload

import co.apraise.util.json.DecoderOps.{decodeKOVs, decodeKvs}
import io.circe.Decoder
import io.circe.Decoder.decodeOption

/**
  * Lex JSON input event (request) decoders. Following the format here:
  * http://docs.aws.amazon.com/lex/latest/dg/lambda-input-response-format.html.
  *
  * Further information is available in the examples:
  * - http://docs.aws.amazon.com/lex/latest/dg/book-trip-detail-flow.html
  * - http://docs.aws.amazon.com/lex/latest/dg/ex1-sch-appt-info-flow-details.html
  */
trait LexInputEventDecoders {
  final val slotsDecoder: Decoder[Map[SlotName, Option[SlotValue]]] =
    Decoder.instance { c =>
      Right(decodeKOVs(c)(SlotName, SlotValue))
    }

  final val intentDecoder: Decoder[Intent] = Decoder.instance { c =>
    for {
      name <- c.downField("name").as[String].map(IntentName)
      slots <- c.downField("slots").as[Map[SlotName, Option[SlotValue]]](slotsDecoder)
      status <- c.downField("confirmationStatus").as[String].map(ConfirmationStatus.fromString)
    } yield Intent(name, slots, status)
  }

  final val botDecoder: Decoder[Bot] = Decoder.instance { c =>
    for {
      name <- c.downField("name").as[String].map(BotName)
      alias <- c.downField("alias").as[Option[String]].map(_.map(BotAlias))
      version <- c.downField("version").as[String].map(BotVersion)
    } yield Bot(name, alias, version)
  }

  final val sessionAttributesDescoder: Decoder[Map[SessionAttributeKey, SessionAttributeValue]] =
    Decoder.instance { c =>
      Right(decodeKvs(c)(SessionAttributeKey, SessionAttributeValue))
    }

  final val lexInputEventDecoder: Decoder[LexInputEvent] = Decoder.instance { c =>
    for {
      intent <- c.downField("currentIntent").as[Intent](intentDecoder)
      bot <- c.downField("bot").as[Bot](botDecoder)
      userId <- c.downField("userId").as[String].map(UserId)
      inputTranscript <- c.downField("inputTranscript").as[Option[String]](decodeOption).map(_.map(InputTranscript))
      invocationSource <- c.downField("invocationSource").as[String].map(InvocationSource.fromString)
      outputDialogMode <- c.downField("outputDialogMode").as[String].map(OutputDialogMode.fromString)
      messageVersion <- c.downField("messageVersion").as[String].map(MessageVersion)
      sessionAttributes <- c.downField("sessionAttributes").as[Map[SessionAttributeKey, SessionAttributeValue]](sessionAttributesDescoder)
    } yield LexInputEvent(
      intent, bot, userId, inputTranscript, invocationSource, outputDialogMode, messageVersion, sessionAttributes
    )
  }
}

object LexInputEventDecoders extends LexInputEventDecoders
