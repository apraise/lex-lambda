package co.apraise.lambda.lex

import shapeless.tag
import shapeless.tag.@@

trait IntentNameTag

trait SlotNameTag

trait SlotValueTag

trait BotNameTag

trait BotAliasTag

trait BotVersionTag

trait UserIdTag

trait InputTranscriptTag

trait MessageVersionTag

trait SessionAttributeKeyTag

trait SessionAttributeValueTag

trait MessageContentTag

trait ResponseCardVersionTag

trait MimeTypeTag

trait CardTitleTag

trait CardSubTitleTag

trait ButtonTextTag

trait ButtonValueTag

package object payload {
  type IntentName = String @@ IntentNameTag

  type SlotName = String @@ SlotNameTag
  type SlotValue = String @@ SlotValueTag

  type BotName = String @@ BotNameTag
  type BotAlias = String @@ BotAliasTag
  type BotVersion = String @@ BotVersionTag

  type UserId = String @@ UserIdTag

  type InputTranscript = String @@ InputTranscriptTag

  type MessageVersion = String @@ MessageVersionTag

  type SessionAttributeKey = String @@ SessionAttributeKeyTag
  type SessionAttributeValue = String @@ SessionAttributeValueTag

  type MessageContent = String @@ MessageContentTag

  type ResponseCardVersion = Int @@ ResponseCardVersionTag

  type MimeType = String @@ MimeTypeTag

  type CardTitle = String @@ CardTitleTag
  type CardSubTitle = String @@ CardSubTitleTag

  type ButtonText = String @@ ButtonTextTag
  type ButtonValue = String @@ ButtonValueTag

  def IntentName(n: String): @@[String, IntentNameTag] = tag[IntentNameTag](n)

  def SlotName(n: String): @@[String, SlotNameTag] = tag[SlotNameTag](n)

  def SlotValue(v: String): @@[String, SlotValueTag] = tag[SlotValueTag](v)

  def BotName(n: String): @@[String, BotNameTag] = tag[BotNameTag](n)

  def BotAlias(a: String): @@[String, BotAliasTag] = tag[BotAliasTag](a)

  def BotVersion(v: String): @@[String, BotVersionTag] = tag[BotVersionTag](v)

  def UserId(i: String): @@[String, UserIdTag] = tag[UserIdTag](i)

  def InputTranscript(t: String): @@[String, InputTranscriptTag] = tag[InputTranscriptTag](t)

  def MessageVersion(v: String): @@[String, MessageVersionTag] = tag[MessageVersionTag](v)

  def SessionAttributeKey(k: String): @@[String, SessionAttributeKeyTag] = tag[SessionAttributeKeyTag](k)

  def SessionAttributeValue(v: String): @@[String, SessionAttributeValueTag] = tag[SessionAttributeValueTag](v)

  def MessageContent(c: String): @@[String, MessageContentTag] = tag[MessageContentTag](c)

  def ResponseCardVersion(v: Int): @@[Int, ResponseCardVersionTag] = tag[ResponseCardVersionTag](v)

  def MimeType(m: String): @@[String, MimeTypeTag] = tag[MimeTypeTag](m)

  def CardTitle(t: String): @@[String, CardTitleTag] = tag[CardTitleTag](t)

  def CardSubTitle(t: String): @@[String, CardSubTitleTag] = tag[CardSubTitleTag](t)

  def ButtonText(t: String): @@[String, ButtonTextTag] = tag[ButtonTextTag](t)

  def ButtonValue(v: String): @@[String, ButtonValueTag] = tag[ButtonValueTag](v)
}
