package co.apraise.lambda.lex.payload

import com.netaporter.uri.Uri

/**
  * Directs Amazon Lex to the next course of action, and describes what to expect from the user after Amazon Lex returns
  * a response to the client.
  */
sealed trait DialogAction

final case class ElicitIntentDialogAction(
    message: Option[Message] = None,
    responseCard: Option[ResponseCard] = None
) extends DialogAction

final case class ElicitSlotDialogAction(
    intentName: IntentName,
    slotToElicit: SlotName,
    slots: Map[SlotName, Option[SlotValue]],
    message: Option[Message] = None,
    responseCard: Option[ResponseCard] = None
) extends DialogAction

final case class ConfirmIntentDialogAction(
    intentName: IntentName,
    slots: Map[SlotName, Option[SlotValue]],
    message: Option[Message] = None,
    responseCard: Option[ResponseCard] = None
) extends DialogAction

final case class DelegateDialogAction(
    slots: Map[SlotName, Option[SlotValue]]
) extends DialogAction

final case class CloseDialogAction(
    fulfillmentState: FulfillmentState,
    message: Option[Message] = None,
    responseCard: Option[ResponseCard] = None
) extends DialogAction

sealed trait FulfillmentState

case object Fulfilled extends FulfillmentState

case object Failed extends FulfillmentState

sealed trait MessageContentType

case object PlainText extends MessageContentType

case object Ssml extends MessageContentType

final case class Message(contentType: MessageContentType, content: MessageContent)

final case class Button(text: ButtonText, value: ButtonValue)

final case class GenericAttachment(
    title: CardTitle, subTitle: CardSubTitle, imageUrl: Uri, attachmentLinkUrl: Uri, buttons: Seq[Button]
)

final case class ResponseCard(version: ResponseCardVersion, attachments: Seq[GenericAttachment])
