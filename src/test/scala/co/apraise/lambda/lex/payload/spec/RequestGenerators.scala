package co.apraise.lambda.lex.payload.spec

import co.apraise.lambda.lex.payload._
import org.scalacheck.Gen

trait RequestGenerators extends CommonGenerators {
  val genBot: Gen[Bot] = for {
    name <- genNonEmptyString.map(BotName)
    alias <- Gen.option(genNonEmptyString.map(BotAlias))
    version <- genNonEmptyString.map(BotVersion)
  } yield Bot(name, alias, version)

  val genInvocationSource: Gen[InvocationSource] = Gen.oneOf(FulfillmentCodeHook, DialogCodeHook)

  val genOutputDialogMode: Gen[OutputDialogMode] = Gen.oneOf(Text, Voice)

  val genLexInputEvent: Gen[LexInputEvent] = for {
    intent <- genIntent
    bot <- genBot
    userId <- genNonEmptyString.map(UserId)
    inputTranscript <- Gen.option(genNonEmptyString.map(InputTranscript))
    invocationSource <- genInvocationSource
    outputDialogMode <- genOutputDialogMode
    messageVersion <- genNonEmptyString.map(MessageVersion)
    sessionAttributes <- genSessionAttributes
  } yield LexInputEvent(
    intent, bot, userId, inputTranscript, invocationSource, outputDialogMode, messageVersion, sessionAttributes
  )
}
