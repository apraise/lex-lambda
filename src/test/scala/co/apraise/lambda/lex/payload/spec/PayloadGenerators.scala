package co.apraise.lambda.lex.payload.spec

trait PayloadGenerators extends RequestGenerators with ResponseGenerators
