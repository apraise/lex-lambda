package co.apraise.lambda.lex.payload.spec

import co.apraise.lambda.lex.payload._
import co.apraise.spec.GeneratorOps.seqOf
import org.scalacheck.Gen

trait ResponseGenerators extends CommonGenerators {
  val genMessageContentType: Gen[MessageContentType] = Gen.oneOf(PlainText, Ssml)

  val genMessageContent: Gen[MessageContent] = genNonEmptyString.map(MessageContent)

  val genMessage: Gen[Message] = for {
    contentType <- genMessageContentType
    content <- genMessageContent
  } yield Message(contentType, content)

  val genResponseCardVersion: Gen[ResponseCardVersion] = Gen.posNum[Int].map(ResponseCardVersion)

  val genButton: Gen[Button] = for {
    text <- genNonEmptyString.map(ButtonText)
    value <- genNonEmptyString.map(ButtonValue)
  } yield Button(text, value)

  val genGenericAttachment: Gen[GenericAttachment] = for {
    title <- genNonEmptyString.map(CardTitle)
    subTitle <- genNonEmptyString.map(CardSubTitle)
    imageUri <- genUri
    attachmentUrl <- genUri
    buttons <- seqOf(genButton)
  } yield GenericAttachment(title, subTitle, imageUri, attachmentUrl, buttons)

  val genResponseCard: Gen[ResponseCard] = for {
    version <- genResponseCardVersion
    attachments <- seqOf(genGenericAttachment)
  } yield ResponseCard(version, attachments)

  val genFulfillmentState: Gen[FulfillmentState] = Gen.oneOf(Fulfilled, Failed)

  val genElicitIntentDialogAction: Gen[ElicitIntentDialogAction] = for {
    message <- Gen.option(genMessage)
    responseCard <- Gen.option(genResponseCard)
  } yield ElicitIntentDialogAction(message, responseCard)

  val genElicitSlotDialogAction: Gen[ElicitSlotDialogAction] = for {
    intent <- genIntentName
    slotToElicit <- genSlotName
    slots <- genSlots
    message <- Gen.option(genMessage)
    responseCard <- Gen.option(genResponseCard)
  } yield ElicitSlotDialogAction(intent, slotToElicit, slots, message, responseCard)

  val genConfirmIntentDialogAction: Gen[ConfirmIntentDialogAction] = for {
    intent <- genIntentName
    slots <- genSlots
    message <- Gen.option(genMessage)
    responseCard <- Gen.option(genResponseCard)
  } yield ConfirmIntentDialogAction(intent, slots, message, responseCard)

  val genDelegateDialogAction: Gen[DelegateDialogAction] = for {
    slots <- genSlots
  } yield DelegateDialogAction(slots)

  val genCloseDialogAction: Gen[CloseDialogAction] = for {
    fulfillmentState <- genFulfillmentState
    message <- Gen.option(genMessage)
    responseCard <- Gen.option(genResponseCard)
  } yield CloseDialogAction(fulfillmentState, message, responseCard)

  val genElicitIntentLexResponse: Gen[LexResponse] = for {
    action <- genElicitIntentDialogAction
    sessionAttributes <- Gen.option(genSessionAttributes)
  } yield ElicitIntentLexResponse(action, sessionAttributes)

  val genElicitSlotLexResponse: Gen[LexResponse] = for {
    action <- genElicitSlotDialogAction
    sessionAttributes <- Gen.option(genSessionAttributes)
  } yield ElicitSlotLexResponse(action, sessionAttributes)

  val genConfirmIntentLexResponse: Gen[LexResponse] = for {
    action <- genConfirmIntentDialogAction
    sessionAttributes <- Gen.option(genSessionAttributes)
  } yield ConfirmIntentLexResponse(action, sessionAttributes)

  val genDelegateLexResponse: Gen[LexResponse] = for {
    action <- genDelegateDialogAction
    sessionAttributes <- genSessionAttributes
  } yield DelegateLexResponse(action, sessionAttributes)

  val genCloseLexResponse: Gen[LexResponse] = for {
    action <- genCloseDialogAction
    sessionAttributes <- Gen.option(genSessionAttributes)
  } yield CloseLexResponse(action, sessionAttributes)

  val genLexResponse: Gen[LexResponse] = Gen.oneOf(
    genElicitIntentLexResponse,
    genElicitSlotLexResponse,
    genConfirmIntentLexResponse,
    genDelegateLexResponse,
    genCloseLexResponse
  )
}
