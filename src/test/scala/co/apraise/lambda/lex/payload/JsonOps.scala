package co.apraise.lambda.lex.payload

import shapeless.tag.@@

trait JsonOps {
  type TaggedString[A] = @@[String, A]

  final def kvsAsJsonObject[K, V](kvs: Map[TaggedString[K], TaggedString[V]]): String =
    kvs.map { case (k, v) => s""" "$k": "$v" """.trim }.mkString("{", ",", "}")

  final def kOptionalVsJsonObject[K, V](sa: Map[TaggedString[K], Option[TaggedString[V]]]): String = {
    sa.map { case (k, ov) =>
      val vOrNull = ov.map(v => s""" "$v" """.trim).orNull
      s""" "$k": $vOrNull """.trim
    }.mkString("{", ",", "}")
  }
}

object JsonOps extends JsonOps
