package co.apraise.lambda.lex.payload

import co.apraise.lambda.lex.payload.LexInputEventDecoders.lexInputEventDecoder
import co.apraise.lambda.lex.payload.spec.PayloadGenerators
import co.apraise.spec.SpecHelper
import co.apraise.util.json.CodecOps
import org.scalacheck.Prop.forAll
import org.scalacheck.Properties
import org.specs2.mutable.Specification

final class InputEventDecodersSpec extends Specification with SpecHelper with PayloadGenerators with InputEventJson {
  val prop = new Properties("Lex Input Event") {
    property("decoding") = forAll(genLexInputEvent) { (event: LexInputEvent) =>
      val decoded = CodecOps.decode(inputEventJson(event))(lexInputEventDecoder)
      decoded must beRight(event)
    }
  }

  s2"Decoding Lex input events${properties(prop)}"
}
