package co.apraise.lambda.lex.payload.spec

import java.time.Instant
import java.time.temporal.ChronoUnit.DAYS

import co.apraise.lambda.lex.apraise.model.Timestamp
import co.apraise.lambda.lex.payload._
import com.netaporter.uri.Uri
import org.scalacheck.Gen

trait CommonGenerators {
  val genNonEmptyString: Gen[String] = Gen.alphaStr.suchThat(i => !i.isEmpty)

  val genUri: Gen[Uri] = Gen.oneOf(
    "http://docs.aws.amazon.com/lex/latest/dg/lambda-input-response-format.html",
    "https://console.aws.amazon.com/lambda/home?region=us-east-1#/functions/lex-lambda?tab=configuration",
    "https://console.aws.amazon.com/lex/home?region=us-east-1"
  ).map(Uri.parse(_))

  val genBigDecimal: Gen[BigDecimal] = Gen.posNum[Double].map(BigDecimal(_))

  val genMillis: Gen[Long] = {
    val now = Instant.now()
    val hundredYearsFuture = now.plus(100 * 365, DAYS)
    Gen.chooseNum(now.toEpochMilli, hundredYearsFuture.toEpochMilli)
  }

  val genInstant: Gen[Instant] = genMillis.map(Instant.ofEpochMilli)

  val genTimestamp: Gen[Timestamp] = genInstant.map(Timestamp(_))

  val genSlotName: Gen[SlotName] = for {
    n <- genNonEmptyString
  } yield SlotName(n)

  val genSlotValue: Gen[SlotValue] = for {
    v <- genNonEmptyString
  } yield SlotValue(v)

  val genSlots: Gen[Map[SlotName, Option[SlotValue]]] = for {
    n <- genSlotName
    v <- Gen.option(genSlotValue)
  } yield Map(n -> v)

  val genSessionAttributes: Gen[Map[SessionAttributeKey, SessionAttributeValue]] = for {
    k <- genNonEmptyString.map(SessionAttributeKey)
    v <- genNonEmptyString.map(SessionAttributeValue)
  } yield Map(k -> v)

  val genIntentName: Gen[IntentName] = genNonEmptyString.map(IntentName)

  val genConfirmationStatus: Gen[ConfirmationStatus] = Gen.oneOf(Unknown, Confirmed, Denied)

  val genIntent: Gen[Intent] = for {
    name <- genIntentName
    slots <- genSlots
    status <- genConfirmationStatus
  } yield Intent(name, slots, status)
}
