package co.apraise.lambda.lex.payload

import co.apraise.lambda.lex.payload.JsonOps.{kvsAsJsonObject, kOptionalVsJsonObject}

trait InputEventJson {
  final def inputEventJson(event: LexInputEvent): String =
    Seq(
      s""" "currentIntent": ${intentJson(event.currentIntent)} """,
      s""" "bot": ${botJson(event.bot)} """,
      s""" "userId": "${event.userId}" """,
      s""" ${event.inputTranscript.map(t => s""" "inputTranscript": "$t" """).getOrElse("")} """,
      s""" "invocationSource": "${invocationSourceValue(event.invocationSource)}" """,
      s""" "outputDialogMode": "${outputDialogValue(event.outputDialogMode)}" """,
      s""" "messageVersion": "${event.messageVersion}" """,
      s""" "sessionAttributes": ${kvsAsJsonObject(event.sessionAttributes)} """
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  final def intentJson(intent: Intent): String =
    s"""
       |{
       |  "name": "${intent.name}",
       |  "slots": ${kOptionalVsJsonObject(intent.slots)},
       |  "confirmationStatus": "${confirmationValue(intent.status)}"
       |}
     """.stripMargin

  final def botJson(bot: Bot): String =
    Seq(
      s""" "name": "${bot.name}" """,
      bot.alias.map(a => s""" "alias": "$a" """).getOrElse(""),
      s""" "version": "${bot.version}" """
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  private def confirmationValue(status: ConfirmationStatus): String =
    status match {
      case Unknown => "None"
      case Confirmed => "Confirmed"
      case Denied => "Denied"
    }

  private def invocationSourceValue(source: InvocationSource) =
    source match {
      case FulfillmentCodeHook => "FulfillmentCodeHook"
      case DialogCodeHook => "DialogCodeHook"
    }

  private def outputDialogValue(outputDialogMode: OutputDialogMode) =
    outputDialogMode match {
      case Text => "Text"
      case Voice => "Voice"
    }
}
