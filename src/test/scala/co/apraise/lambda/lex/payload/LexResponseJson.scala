package co.apraise.lambda.lex.payload

import co.apraise.lambda.lex.payload.JsonOps.{kOptionalVsJsonObject, kvsAsJsonObject}

trait LexResponseJson {
  final def lexResponseJson(lexResponse: LexResponse): String =
    lexResponse match {
      case eilr: ElicitIntentLexResponse => elicitIntentLexResponseJson(eilr)
      case eslr: ElicitSlotLexResponse => elicitSlotLexResponseJson(eslr)
      case cilr: ConfirmIntentLexResponse => confirmIntentLexResponseJson(cilr)
      case dlr: DelegateLexResponse => delegateLexResponseJson(dlr)
      case clr: CloseLexResponse => closeLexResponseJson(clr)
    }

  private def elicitIntentLexResponseJson(eilr: ElicitIntentLexResponse): String =
    Seq(
      s""" "dialogAction": ${elicitIntentDialogActionJson(eilr.dialogAction)} """,
      eilr.sessionAttributes.map(sa => s""" "sessionAttributes": ${kvsAsJsonObject(sa)} """).getOrElse("")
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  private def elicitSlotLexResponseJson(eslr: ElicitSlotLexResponse): String =
    Seq(
      s""" "dialogAction": ${elicitSlotDialogActionJson(eslr.dialogAction)} """,
      eslr.sessionAttributes.map(sa => s""" "sessionAttributes": ${kvsAsJsonObject(sa)} """).getOrElse("")
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  private def confirmIntentLexResponseJson(cilr: ConfirmIntentLexResponse): String =
    Seq(
      s""" "dialogAction": ${confirmIntentDialogActionJson(cilr.dialogAction)} """,
      cilr.sessionAttributes.map(sa => s""" "sessionAttributes": ${kvsAsJsonObject(sa)} """).getOrElse("")
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  private def delegateLexResponseJson(dlr: DelegateLexResponse): String =
    s"""
       |{
       |  "sessionAttributes": ${kvsAsJsonObject(dlr.sessionAttributes)},
       |  "dialogAction": ${delegateDialogActionJson(dlr.dialogAction)}
       |}
     """.stripMargin

  private def closeLexResponseJson(clr: CloseLexResponse): String =
    Seq(
      s""" "dialogAction": ${closeDialogActionJson(clr.dialogAction)} """,
      clr.sessionAttributes.map(sa => s""" "sessionAttributes": ${kvsAsJsonObject(sa)} """).getOrElse("")
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  private def elicitIntentDialogActionJson(dialogAction: ElicitIntentDialogAction) =
    Seq(
      """ "type": "ElicitIntent" """,
      dialogAction.message.map(m => s""" "message": ${messageJson(m)} """).getOrElse(""),
      dialogAction.responseCard.map(rc => s""" "responseCard": ${responseCardJson(rc)} """).getOrElse("")
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  private def elicitSlotDialogActionJson(dialogAction: ElicitSlotDialogAction) =
    Seq(
      """ "type": "ElicitSlot" """,
      s""" "intentName": "${dialogAction.intentName}" """,
      s""" "slots": ${kOptionalVsJsonObject(dialogAction.slots)} """,
      s""" "slotToElicit": "${dialogAction.slotToElicit}" """,
      dialogAction.message.map(m => s""" "message": ${messageJson(m)} """).getOrElse(""),
      dialogAction.responseCard.map(rc => s""" "responseCard": ${responseCardJson(rc)} """).getOrElse("")
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  private def confirmIntentDialogActionJson(dialogAction: ConfirmIntentDialogAction) =
    Seq(
      """ "type": "ConfirmIntent" """,
      s""" "intentName": "${dialogAction.intentName}" """,
      s""" "slots": ${kOptionalVsJsonObject(dialogAction.slots)} """,
      dialogAction.message.map(m => s""" "message": ${messageJson(m)} """).getOrElse(""),
      dialogAction.responseCard.map(rc => s""" "responseCard": ${responseCardJson(rc)} """).getOrElse("")
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  private def delegateDialogActionJson(dialogAction: DelegateDialogAction) =
    Seq(
      """ "type": "Delegate" """,
      s""" "slots": ${kOptionalVsJsonObject(dialogAction.slots)} """
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  private def closeDialogActionJson(dialogAction: CloseDialogAction) =
    Seq(
      """ "type": "Close" """,
      s""" "fulfillmentState": "${fulfillmentStateValueJson(dialogAction.fulfillmentState)}" """,
      dialogAction.message.map(m => s""" "message": ${messageJson(m)} """).getOrElse(""),
      dialogAction.responseCard.map(rc => s""" "responseCard": ${responseCardJson(rc)} """).getOrElse("")
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  private def messageJson(m: Message) =
    s"""
       |{
       |  "contentType": "${contentTypeValueJson(m.contentType)}",
       |  "content": "${m.content}"
       |}
     """.stripMargin

  private def responseCardJson(rc: ResponseCard) =
    Seq(
      s""" "version": ${rc.version} """,
      """ "contentType": "application/vnd.amazonaws.card.generic" """,
      s""" "genericAttachments": ${rc.attachments.map(genericAttachmentJson).mkString("[", ",", "]")} """
    ).map(_.trim).filter(_.nonEmpty).mkString("{", ",", "}")

  private def genericAttachmentJson(a: GenericAttachment) =
    s"""
       |{
       |  "title": "${a.title}",
       |  "subTitle": "${a.subTitle}",
       |  "imageUrl": "${a.imageUrl.toString()}",
       |  "attachmentLinkUrl": "${a.attachmentLinkUrl.toString()}",
       |  "buttons": [${a.buttons.map(buttonJson).mkString(",")}]
       |}
     """.stripMargin

  private def buttonJson(b: Button) =
    s"""
       |{
       |  "text": "${b.text}",
       |  "value": "${b.value}"
       |}
     """.stripMargin

  private def contentTypeValueJson(ct: MessageContentType) =
    ct match {
      case PlainText => "PlainText"
      case Ssml => "SSML"
    }

  private def fulfillmentStateValueJson(fulfillmentState: FulfillmentState) =
    fulfillmentState match {
      case Fulfilled => "Fulfilled"
      case Failed => "Failed"
    }
}
