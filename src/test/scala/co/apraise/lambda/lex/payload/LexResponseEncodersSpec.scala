package co.apraise.lambda.lex.payload

import co.apraise.lambda.lex.payload.LexReponseEncoders.lexResponseEncoder
import co.apraise.lambda.lex.payload.spec.PayloadGenerators
import co.apraise.spec.SpecHelper
import co.apraise.util.json.JsonPrinter.jsonToString
import org.scalacheck.Prop.forAll
import org.scalacheck.Properties
import org.specs2.mutable.Specification

final class LexResponseEncodersSpec extends Specification with SpecHelper with PayloadGenerators with LexResponseJson {
  val prop = new Properties("Lex Response") {
    property("encoding") = forAll(genLexResponse) { (response: LexResponse) =>
      val encoded = parse(jsonToString(encode(response)(lexResponseEncoder)))
      encoded must beEqualTo(parse(lexResponseJson(response)))
    }
  }

  s2"Encoding Lex responses${properties(prop)}"
}
