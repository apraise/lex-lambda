package co.apraise.lambda.lex.apraise.backend

import co.apraise.lambda.lex.apraise.model.PropertyValuation

trait MarketValueJson {
  final def marketValueResponse(valuation: PropertyValuation): String =
    s"""
       |{
       |  "data": {
       |    "marketValue": {
       |      "property": {
       |        "address": {
       |          "canonicalForm": "${valuation.property.address.canonicalForm}"
       |        }
       |      },
       |      "estimate": {
       |        "amount": {
       |          "amount": ${valuation.valuation.amount.amount.toString()}
       |        },
       |        "when": {
       |          "epochMillis": ${valuation.valuation.when.instant.toEpochMilli}
       |        }
       |      }
       |    }
       |  }
       |}
     """.stripMargin

  final def marketValueErrorResponse(error: Exception): String =
    s"""
       |{
       |  "data": {},
       |  "errors": [
       |    {
       |      "message": "co.apraise.core.InputParseError: Unable to parse address from '26 Lygon St'",
       |      "type": "co.apraise.core.InputParseError",
       |      "path": [
       |        "marketValue"
       |      ],
       |      "locations": [
       |        {
       |          "line": 2,
       |          "column": 3
       |        }
       |      ]
       |    }
       |  ]
       |}
     """.stripMargin
}
