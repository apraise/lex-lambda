package co.apraise.lambda.lex.apraise.backend

import co.apraise.lambda.lex.apraise.model._
import co.apraise.lambda.lex.payload.spec.CommonGenerators
import org.scalacheck.Gen

trait MarketValueGenerators extends CommonGenerators {
  val genMonetaryAmount: Gen[MonetaryAmount] = genBigDecimal.map(MonetaryAmount(_))

  val genCanonicalAddress: Gen[CanonicalAddress] = genNonEmptyString.map(CanonicalAddress)

  val genAddress: Gen[Address] =
    for {
      canonicalForm <- genCanonicalAddress
    } yield Address(canonicalForm)

  val genProperty: Gen[Property] =
    for {
      address <- genAddress
    } yield Property(address)

  val genValuationEstimate: Gen[ValuationEstimate] =
    for {
      amount <- genMonetaryAmount
      when <- genTimestamp
    } yield ValuationEstimate(amount, when)

  val genPropertyValuation: Gen[PropertyValuation] =
    for {
      property <- genProperty
      valuation <- genValuationEstimate
    } yield PropertyValuation(property, valuation)

  val genApraiseError: Gen[Exception] = genNonEmptyString.map(message => new Exception(message))
}
