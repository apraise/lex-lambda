package co.apraise.lambda.lex.apraise.backend

import co.apraise.lambda.lex.apraise.backend.GraphQlCodecs.marketValueResponseDecoder
import co.apraise.lambda.lex.apraise.model.PropertyValuation
import co.apraise.spec.SpecHelper
import co.apraise.util.json.CodecOps
import org.scalacheck.Prop.forAll
import org.scalacheck.Properties
import org.specs2.mutable.Specification

final class GraphQlCodecsSpec extends Specification with SpecHelper with MarketValueGenerators with MarketValueJson {
  val prop = new Properties("Property market valuations") {
    property("success decoding") = forAll(genPropertyValuation) { (valuation: PropertyValuation) =>
      val decoded = CodecOps.decode(marketValueResponse(valuation))(marketValueResponseDecoder)
      decoded must beEqualTo(Right(Right(valuation)))
    }
    property("failure decoding") = forAll(genApraiseError) { (error: Exception) =>
      val decoded = CodecOps.decode(marketValueErrorResponse(error))(marketValueResponseDecoder)
      decoded must beEqualTo(Right(Left(Seq(ParseError))))
    }
  }

  s2"Decoding property valuations${properties(prop)}"
}
