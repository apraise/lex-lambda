package co.apraise.spec

import co.apraise.util.async.singleThreadedExecutionContext
import co.apraise.util.json.CodecOps
import co.apraise.util.log.Logger
import org.specs2.ScalaCheck
import org.specs2.execute.{Failure, Result}
import org.specs2.mutable.Specification
import org.specs2.specification.BeforeAll

trait SpecLogging {
  final val log = new Logger("lambda-lex-test")(singleThreadedExecutionContext)
}

object SpecLogging extends SpecLogging

trait SpecHelper extends SpecLogging
    with ScalaCheck with CodecOps with BeforeAll { self: Specification =>
  override def beforeAll() = TestEnvironmentSetter.setEnvironment()

  final def fail(message: String, t: Throwable): Result = Failure(message, "", t.getStackTrace.toList)
}
