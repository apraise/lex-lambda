package co.apraise.spec

import cats.data.NonEmptyList
import cats.data.NonEmptyList.fromListUnsafe
import org.scalacheck.Gen

trait GeneratorOps {
  final def nonEmptyListOfN[A](n: Int, gen: Gen[A]): Gen[NonEmptyList[A]] =
    Gen.listOfN(n, gen).suchThat(as => as.nonEmpty).map(fromListUnsafe)

  final def seqOfMaxN[A](n: Int, gen: Gen[A]): Gen[Seq[A]] =
    Gen.oneOf(Gen.const(Seq.empty), Gen.listOfN(n, gen))

  final def seqOf[A](gen: Gen[A]): Gen[Seq[A]] = Gen.choose(1, 5).flatMap(n => seqOfMaxN(n, gen))
}

object GeneratorOps extends GeneratorOps
