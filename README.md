# Apraise Lambda for AWS Lex bot

A Lambda for the Apraise Lex bot.

# Setup

To setup for local development, run these steps.

1. Install [Java 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) from Oracle.
   You will need a JDK (not a JRE), as of the time of writing this is "Java SE Development Kit 8u92". There is also
   [documentation](http://www.oracle.com/technetwork/java/javase/documentation/jdk8-doc-downloads-2133158.html)
   available (handy for linking into your IDE).

1. Run [sbt](http://www.scala-sbt.org/release/docs/Getting-Started/Setup.html):

    ```
    $ ./sbt
    > update
    ```

    Note. You could also `brew install sbt` if you'd prefer a system version.

1. `etc/development-env` contains a template `.env` file that you can use to configure your environment variables locally:

    ```
    $ cp etc/development.env .env
    ```

  See [sbt-dotenv](https://github.com/mefellows/sbt-dotenv) for more information. *Do not check `.env` into source control!*.

  Note that if you change this while an sbt session is running, you will need to `reload` sbt for the new settings to
  take effect (as it's an sbt plugin).

1. If you're using IntelliJ for development, grab the Scala plugin, and import the project (File->Open, or Import from
   the welcome screen) as an SBT project. If you want to run tests, you will need to add the environment variables (in
   `.env`) to the run configurations (specs2).

# Testing

```
$ ./sbt test
```

This will start the `sbt` REPL, from where you can issue [commands](http://www.scala-sbt.org/0.13/docs/Running.html#Common+commands).

* `test` - Runs all tests;
* `test-only co.apraise.core.CurrencySpec` - Runs a single test;
* `test-only co.apraise.core.*` - Runs all tests in the `co.apraise.core` package;
* `test-only *CurrencySpec` - Runs `CurrencySpec`;
* `test:compile` - Compiles your test code.

Appending a `~` to the start of any sbt command will run it continuously; for example to run tests continuously:

```
> ~test
```

# Deployment

Deployment is done by uploading a built binary to AWS S3, into the `apraise-production-lambda` bucket.

```
$ ./sbt
> deploy
```

Then, using the [Lambda Console](https://console.aws.amazon.com/lambda/home?region=us-east-1), you will need to
manually upload the latest code, the URL will look something like:

```
https://s3.amazonaws.com/apraise-production-lambda/apraise-lambda-lex-assembly-0.0.1-SNAPSHOT.jar
```

## Uninstall

You can uninstall everything you installed for this project by:

```
$ rm -rf ~/.sbt
$ rm -rf ~/.ivy2
```

Then, if you want, you can uninstall Java by following the instructions here: https://docs.oracle.com/javase/8/docs/technotes/guides/install/mac_jdk.html#A1096903
